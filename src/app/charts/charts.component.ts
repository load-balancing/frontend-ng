import {AfterViewInit, Component, ComponentFactoryResolver, ViewChild, ViewContainerRef} from '@angular/core';
import {ChartComponent} from '../chart/chart.component';
import {LoadWebSocketService} from '../rest-api/load/load-web-socket.service';
import {ILoadResponse} from '../rest-api/load/model/load-response.interface';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements AfterViewInit {

  @ViewChild('chartContainer', {read: ViewContainerRef}) entry: ViewContainerRef;
  charts = new Map<string, ChartComponent>();

  constructor(private resolver: ComponentFactoryResolver,
              private loadWebSocketService: LoadWebSocketService) {
  }

  ngAfterViewInit(): void {
    this.loadWebSocketService.loadObservable.subscribe(it => this.handleLoadEvent(JSON.parse(it.body)));
  }

  handleLoadEvent(load: ILoadResponse) {
    const existingChart = this.charts.get(load.instanceId);

    if (existingChart) {
      existingChart.addPoint(load.cpuLoad);
    } else {
      this.addChart(load.instanceId);
    }
  }

  private addChart(instanceId: string) {
    const factory = this.resolver.resolveComponentFactory(ChartComponent);
    const componentRef = this.entry.createComponent(factory);
    componentRef.instance.instanceId = instanceId;
    this.charts.set(instanceId, componentRef.instance);
  }


}
