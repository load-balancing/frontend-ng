import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-task-expand',
  templateUrl: './task-expand.component.html',
  styleUrls: ['./task-expand.component.css']
})
export class TaskExpandComponent implements OnInit {

  folded = true;

  constructor() {
  }

  ngOnInit(): void {
  }

}
