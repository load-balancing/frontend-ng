import {InjectableRxStompConfig} from '@stomp/ng2-stompjs';
import {environment} from '../../environments/environment';

export const myRxStompConfig: InjectableRxStompConfig = {
  brokerURL: `${environment.webSocketUrl}/ws`,
  reconnectDelay: 200,

  debug: (msg: string): void => {
    // console.log(new Date(), msg);
  }
};

