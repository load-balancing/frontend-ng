import {Component, OnInit} from '@angular/core';
import {ITaskResponse} from '../rest-api/task/model/task-response.interface';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  tasks: ITaskResponse[];

  constructor() {
  }

  ngOnInit(): void {
  }

}
