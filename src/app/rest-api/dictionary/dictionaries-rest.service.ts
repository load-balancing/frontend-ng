import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable()
export class DictionariesRestService {

  dictionariesUrl = `${environment.serverUrl}/dictionary`;

  constructor(private httpClient: HttpClient) {
  }

  fetchAlgorithmsDictionary(): Observable<string[]> {
    return this.httpClient.get<string[]>(`${this.dictionariesUrl}/algorithms`);
  }

  fetchTaskTypesDictionary(): Observable<string[]> {
    return this.httpClient.get<string[]>(`${this.dictionariesUrl}/task-types`);
  }

}
