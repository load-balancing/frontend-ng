import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ITaskCommand} from './model/task-command.interface';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class TaskRestService {

  taskUri = `${environment.serverUrl}/instances`;

  constructor(private httpClient: HttpClient) {
  }

  invoikeTask(command: ITaskCommand): Observable<any> {
    return this.httpClient.post(`${this.taskUri}/task/invoke`, command);
  }

}
