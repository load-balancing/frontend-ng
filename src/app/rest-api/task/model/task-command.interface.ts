export interface ITaskCommand {
  tasksPerInstance: number;
  totalTasksCount: number;
  taskType: string;
  algorithm: string;
}
