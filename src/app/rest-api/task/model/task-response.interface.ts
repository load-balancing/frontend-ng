export interface ITaskResponse {
  taskId: string;
  startTime: any;
  endTime: any;
  executionTime: any;
}

export interface IInstanceTask {
  instanceId: string;
  executionTime: number;
}
