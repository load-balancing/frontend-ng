export interface ILoadResponse {
  instanceId: string;
  cpuLoad: number;
}
