import {Injectable} from '@angular/core';
import {RxStompService} from '@stomp/ng2-stompjs';

@Injectable()
export class LoadWebSocketService {

  get loadObservable() {
    return this.rxStompService.watch('/topic/load');
  }

  constructor(private rxStompService: RxStompService) {
  }

}
