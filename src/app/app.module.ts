import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {ChartsComponent} from './charts/charts.component';
import {HighchartsChartModule} from 'highcharts-angular';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {LoadWebSocketService} from './rest-api/load/load-web-socket.service';
import {InjectableRxStompConfig, RxStompService, rxStompServiceFactory} from '@stomp/ng2-stompjs';
import {myRxStompConfig} from './configuration/rx-stomp-config';
import {ChartComponent} from './chart/chart.component';
import {NavbarComponent} from './navbar/navbar.component';
import {MainComponent} from './main/main.component';
import {TasksComponent} from './tasks/tasks.component';
import {TaskExpandComponent} from './task-expand/task-expand.component';
import {TableModule} from 'primeng/table';
import {TaskDialogComponent} from './task-dialog/task-dialog.component';
import {DialogModule} from 'primeng';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {DictionariesRestService} from './rest-api/dictionary/dictionaries-rest.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TaskRestService} from './rest-api/task/task-rest.service';

const appRoutes: Routes = [
  {path: 'main', component: MainComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    ChartsComponent,
    ChartComponent,
    NavbarComponent,
    MainComponent,
    TasksComponent,
    TaskExpandComponent,
    TaskDialogComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'}),
    HighchartsChartModule,
    HttpClientModule,
    TableModule,
    DialogModule,
    BrowserAnimationsModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),  ],
  providers: [
    {
      provide: InjectableRxStompConfig,
      useValue: myRxStompConfig
    },
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
      deps: [InjectableRxStompConfig]
    },
    LoadWebSocketService,
    DictionariesRestService,
    TaskRestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
