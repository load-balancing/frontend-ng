import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DictionariesRestService} from '../rest-api/dictionary/dictionaries-rest.service';
import {ITaskCommand} from '../rest-api/task/model/task-command.interface';
import {TaskRestService} from '../rest-api/task/task-rest.service';

@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.css']
})
export class TaskDialogComponent implements OnInit {

  visible = false;
  taskTypes: string[];
  algorithms: string[];
  command = {} as ITaskCommand;

  @Output()
  onNewTask = new EventEmitter();

  constructor(private dictionariesRestService: DictionariesRestService,
              private taskRestService: TaskRestService) {
  }

  ngOnInit(): void {
    this.dictionariesRestService.fetchTaskTypesDictionary().subscribe(
      it => this.taskTypes = it,
      () => console.log('Error loading task types')
    );
    this.dictionariesRestService.fetchAlgorithmsDictionary().subscribe(
      it => this.algorithms = it,
      () => console.log('Error loading algorithms')
    );
  }

  closeDialog() {
    this.visible = false;
  }

  public showDialog() {
    this.visible = true;
  }

  onSubmit() {
    this.taskRestService.invoikeTask(this.command)
      .subscribe(it => {
        this.onNewTask.emit(null);
        this.closeDialog();
      });
  }

}
