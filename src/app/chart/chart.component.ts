import {Component, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  instanceId: string;

  Highcharts = Highcharts;

  chartOptions: any;
  chart: any;

  pointCount = 0;

  ngOnInit(): void {
    this.chartOptions = {
      series: [{
        data: [0],
        type: 'areaspline',
        name: 'Cpu usage'
      }],
      title: {
        text: this.instanceId
      },
      xAxis: {},
      yAxis: {
        min: 0,
        max: 1
      }
    };
  }

  initializeInstance(event) {
    this.chart = event;
  }

  addPoint(point: number) {
    this.chart.series[0].addPoint(point, true, this.shouldShift());
    this.pointCount += 1;
  }

  private shouldShift(): boolean {
    console.log(this.pointCount);
    return this.pointCount > 35;
  }

}
